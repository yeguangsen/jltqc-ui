import request from '@/utils/request'

export function u8query(data) {
  return request({
    url: '/complaint/query',
    method: 'post',
    data
  })
}
export function query(data) {
  return request({
    url: '/complaint/query',
    method: 'post',
    data
  })
}
export function save(data) {
  return request({
    url: '/complaint/save',
    method: 'post',
    data
  })
}
export function delbyid(id) {
  return request({
    url: '/complaint/del',
    method: 'get',
    params: { id: id }
  })
}
export function crmCustomerPoolSetListAPI(id) {
  return request({
    url: '/complaint/del',
    method: 'get',
    params: { id: id }
  })
}
export function get(id) {
  return request({
    url: '/complaint/get',
    method: 'get',
    params: { id: id }
  })
}
export function getLast(id) {
  return request({
    url: '/complaint/getLast',
    method: 'get',
    params: { id: id }
  })
}
export function getNext(id) {
  return request({
    url: '/complaint/getNext',
    method: 'get',
    params: { id: id }
  })
}
