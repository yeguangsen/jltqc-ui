import request from '@/utils/request'

export function query(data) {
  return request({
    url: '/rpProduce/query',
    method: 'post',
    data
  })
}
export function save(data) {
  return request({
    url: '/rpProduce/save',
    method: 'post',
    data
  })
}
export function delbyid(id) {
  return request({
    url: '/rpProduce/del',
    method: 'get',
    params: { id: id }
  })
}
export function get(id) {
  return request({
    url: '/rpProduce/get',
    method: 'get',
    params: { id: id }
  })
}
export function generateWorkflow(id) {
  return request({
    url: '/rpProduce/generateWorkflow',
    method: 'get',
    params: { id: id }
  })
}
export function getLast(id) {
  return request({
    url: '/rpProduce/getLast',
    method: 'get',
    params: { id: id }
  })
}
export function getNext(id) {
  return request({
    url: '/rpProduce/getNext',
    method: 'get',
    params: { id: id }
  })
}
export function findRpProduceBySubBarcode(ppbarcode) {
  return request({
    url: '/rpProduce/findRpProduceBySubBarcode',
    method: 'get',
    params: { ppbarcode: ppbarcode }
  })
}
export function findRpProduceByBarcode(ppbarcode) {
  return request({
    url: '/rpProduce/findRpProduceBySubBarcode',
    method: 'get',
    params: { ppbarcode: ppbarcode }
  })
}

