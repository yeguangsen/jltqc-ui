import request from '@/utils/request'

export function u8query(data) {
  return request({
    url: '/u8OutCome/query',
    method: 'post',
    data
  })
}
export function query(data) {
  return request({
    url: '/outCome/query',
    method: 'post',
    data
  })
}
export function save(data) {
  return request({
    url: '/outCome/save',
    method: 'post',
    data
  })
}
export function delbyid(id) {
  return request({
    url: '/outCome/del',
    method: 'get',
    params: { id: id }
  })
}
export function crmCustomerPoolSetListAPI(id) {
  return request({
    url: '/outCome/del',
    method: 'get',
    params: { id: id }
  })
}
export function get(id) {
  return request({
    url: '/outCome/get',
    method: 'get',
    params: { id: id }
  })
}
export function getLast(id) {
  return request({
    url: '/outCome/getLast',
    method: 'get',
    params: { id: id }
  })
}
export function getNext(id) {
  return request({
    url: '/outCome/getNext',
    method: 'get',
    params: { id: id }
  })
}
