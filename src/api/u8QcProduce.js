import request from '@/utils/request'

export function get(id) {
  return request({
    url: '/u8QcProduce/get',
    method: 'get',
    params: { id: id }
  })
}

