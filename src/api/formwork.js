import request from '@/utils/request'

export function save(data) {
  return request({
    url: '/formwork/save',
    method: 'post',
    data
  })
}

export function query(data) {
  return request({
    url: '/formwork/query',
    method: 'post',
    data
  })
}

export function delbyid(id) {
  return request({
    url: '/formwork/del',
    method: 'get',
    params: { id: id }
  })
}
export function get(id) {
  return request({
    url: '/formwork/get',
    method: 'get',
    params: { id: id }
  })
}
export function getFormworkByMold(mold) {
  return request({
    url: '/formwork/getFormworkByMold',
    method: 'get',
    params: { mold: mold }
  })
}
export function getLast(id, mold) {
  return request({
    url: '/formwork/getLast',
    method: 'get',
    params: { id: id, mold: mold }
  })
}
export function getNext(id, mold) {
  return request({
    url: '/formwork/getNext',
    method: 'get',
    params: { id: id, mold: mold }
  })
}

