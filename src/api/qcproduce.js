import request from '@/utils/request'

export function u8query(data) {
  return request({
    url: '/u8QcProduce/query',
    method: 'post',
    data
  })
}
export function finish(id) {
  return request({
    url: '/u8QcProduce/finish',
    method: 'get',
    params: { id: id }
  })
}
export function query(data) {
  return request({
    url: '/qcProduce/query',
    method: 'post',
    data
  })
}
export function save(data) {
  return request({
    url: '/qcProduce/save',
    method: 'post',
    data
  })
}
export function delbyid(id) {
  return request({
    url: '/qcProduce/del',
    method: 'get',
    params: { id: id }
  })
}
export function crmCustomerPoolSetListAPI(id) {
  return request({
    url: '/qcProduce/del',
    method: 'get',
    params: { id: id }
  })
}
export function get(id) {
  return request({
    url: '/qcProduce/get',
    method: 'get',
    params: { id: id }
  })
}
export function generateWorkflow(id) {
  return request({
    url: '/qcProduce/generateWorkflow',
    method: 'get',
    params: { id: id }
  })
}
export function getLast(id) {
  return request({
    url: '/qcProduce/getLast',
    method: 'get',
    params: { id: id }
  })
}
export function getNext(id) {
  return request({
    url: '/qcProduce/getNext',
    method: 'get',
    params: { id: id }
  })
}
