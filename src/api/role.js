import request from '@/utils/request'

export function getRoles(params) {
  return request({
    url: '/baseRole/findRoleHaveMenuList',
    method: 'post',
    params
  })
}
export function getAllRoles(params) {
  return request({
    url: '/baseRole/getAllRoles',
    method: 'get'
  })
}
export function saveRole(data) {
  return request({
    url: '/baseRole/save',
    method: 'post',
    data
  })
}
export function searchMenuById(params) {
  return request({
    url: '/baseRole/save',
    method: 'post',
    params
  })
}

