import request from '@/utils/request'

export function query(data) {
  return request({
    url: '/u8Vendor/query',
    method: 'post',
    data
  })
}

