import request from '@/utils/request'

export function save(data) {
  return request({
    url: '/baseRole/save',
    method: 'post',
    data
  })
}

export function query(data) {
  return request({
    url: '/baseRole/query',
    method: 'post',
    data
  })
}
export function list(data) {
  return request({
    url: '/baseRole/list',
    method: 'get'
  })
}

export function delbyid(id) {
  return request({
    url: '/baseRole/delbyid',
    method: 'get',
    params: { id: id }
  })
}
export function crmCustomerPoolSetListAPI(id) {
  return request({
    url: '/baseRole/delbyid',
    method: 'get',
    params: { id: id }
  })
}


