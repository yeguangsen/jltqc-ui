import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/baseUser/login',
    method: 'post',
    params: { 'username': data.username, 'password': data.password }
  })
}

export function delbyid(data) {
  return request({
    url: '/baseUser/delbyid',
    method: 'get',
    params: { 'id': data }
  })
}
export function getInfo(token) {
  return request({
    url: '/baseUser/info',
    method: 'get',
    params: { token }
  })
}
export function get() {
  return request({
    url: '/baseUser/get',
    method: 'get'
  })
}
export function logout() {
  return request({
    url: '/baseUser/logout',
    method: 'post'
  })
}
export function query(data) {
  return request({
    url: '/baseUser/query',
    method: 'post',
    data
  })
}
export function saveUserAndRoles(data) {
  return request({
    url: '/baseUser/saveUserAndRoles',
    method: 'post',
    data
  })
}

