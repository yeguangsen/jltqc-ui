import request from '@/utils/request'

export function save(data) {
  return request({
    url: '/project/save',
    method: 'post',
    data
  })
}

export function query(data) {
  return request({
    url: '/project/query',
    method: 'post',
    data
  })
}
export function get(id) {
  return request({
    url: '/project/get',
    method: 'get',
    params: { id: id }
  })
}
export function getLast(id) {
  return request({
    url: '/project/getLast',
    method: 'get',
    params: { id: id }
  })
}
export function getNext(id) {
  return request({
    url: '/project/getNext',
    method: 'get',
    params: { id: id }
  })
}
export function getByCInvCode(cinvcode) {
  return request({
    url: '/project/getByCInvCode',
    method: 'get',
    params: { cinvcode: cinvcode }
  })
}
export function delbyid(id) {
  return request({
    url: '/project/del',
    method: 'get',
    params: { id: id }
  })
}
export function crmCustomerPoolSetListAPI(id) {
  return request({
    url: '/project/delbyid',
    method: 'get',
    params: { id: id }
  })
}


