import request from '@/utils/request'

export function save(data) {
  return request({
    url: '/workflowresult/save',
    method: 'post',
    data
  })
}
