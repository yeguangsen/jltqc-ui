import request from '@/utils/request'

export function query(data) {
  return request({
    url: '/rpStock/query',
    method: 'post',
    data
  })
}
export function save(data) {
  return request({
    url: '/rpStock/save',
    method: 'post',
    data
  })
}
export function delbyid(id) {
  return request({
    url: '/rpStock/del',
    method: 'get',
    params: { id: id }
  })
}
export function get(id) {
  return request({
    url: '/rpStock/get',
    method: 'get',
    params: { id: id }
  })
}
export function generateWorkflow(id) {
  return request({
    url: '/rpStock/generateWorkflow',
    method: 'get',
    params: { id: id }
  })
}
export function getLast(id) {
  return request({
    url: '/rpStock/getLast',
    method: 'get',
    params: { id: id }
  })
}
export function getNext(id) {
  return request({
    url: '/rpStock/getNext',
    method: 'get',
    params: { id: id }
  })
}
