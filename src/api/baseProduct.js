import request from '@/utils/request'

export function save(data) {
  return request({
    url: '/baseProduct/save',
    method: 'post',
    data
  })
}

export function query(data) {
  return request({
    url: '/baseProduct/query',
    method: 'post',
    data
  })
}

export function delbyid(id) {
  return request({
    url: '/baseProduct/del',
    method: 'get',
    params: { id: id }
  })
}
export function crmCustomerPoolSetListAPI(id) {
  return request({
    url: '/baseProduct/delbyid',
    method: 'get',
    params: { id: id }
  })
}



