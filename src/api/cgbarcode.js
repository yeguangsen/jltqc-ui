import request from '@/utils/request'

export function findAllByAutoId(autoid) {
  return request({
    url: '/cgbarcode/findAllByAutoId',
    method: 'get',
    params: { autoid }
  })
}


