import request from '@/utils/request'

export function query(data) {
  return request({
    url: '/inCome/query',
    method: 'post',
    data
  })
}

export function save(data) {
  return request({
    url: '/inCome/save',
    method: 'post',
    data
  })
}
export function delbyid(id) {
  return request({
    url: '/inCome/del',
    method: 'get',
    params: { id: id }
  })
}
export function finish(id) {
  return request({
    url: '/inCome/finish',
    method: 'get',
    params: { id: id }
  })
}
export function crmCustomerPoolSetListAPI(id) {
  return request({
    url: '/inCome/del',
    method: 'get',
    params: { id: id }
  })
}
export function get(id) {
  return request({
    url: '/inCome/get',
    method: 'get',
    params: { id: id }
  })
}
