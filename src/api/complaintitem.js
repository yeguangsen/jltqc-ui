import request from '@/utils/request'

export function save(data) {
  return request({
    url: '/complaintItem/save',
    method: 'post',
    data
  })
}

export function getMenus(params) {
  return request({
    url: '/complaintItem/getMenus',
    method: 'post',
    params
  })
}
export function list() {
  return request({
    url: '/complaintItem/list',
    method: 'get'
  })
}

export function delbyid(id) {
  return request({
    url: '/complaintItem/del',
    method: 'get',
    params: { id: id }
  })
}

export function query(data) {
  return request({
    url: '/complaintItem/query',
    method: 'post',
    data
  })
}
