import request from '@/utils/request'

export function findAll(id) {
  return request({
    url: '/qmilcode/findAll',
    method: 'get'
  })
}
export function get(data) {
  return request({
    url: '/qmilcode/get',
    method: 'get',
    params: data
  })
}
export function findDemoByLevelAndQty(data) {
  return request({
    url: '/qmilcode/findDemoByLevelAndQty',
    method: 'get',
    params: data
  })
}
