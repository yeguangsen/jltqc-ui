import request from '@/utils/request'

export function query(data) {
  return request({
    url: '/rpXunjian/query',
    method: 'post',
    data
  })
}
export function save(data) {
  return request({
    url: '/rpXunjian/save',
    method: 'post',
    data
  })
}
export function delbyid(id) {
  return request({
    url: '/rpXunjian/del',
    method: 'get',
    params: { id: id }
  })
}
export function get(id) {
  return request({
    url: '/rpXunjian/get',
    method: 'get',
    params: { id: id }
  })
}
export function generateWorkflow(id) {
  return request({
    url: '/rpXunjian/generateWorkflow',
    method: 'get',
    params: { id: id }
  })
}
export function getLast(id) {
  return request({
    url: '/rpXunjian/getLast',
    method: 'get',
    params: { id: id }
  })
}
export function getNext(id) {
  return request({
    url: '/rpXunjian/getNext',
    method: 'get',
    params: { id: id }
  })
}
