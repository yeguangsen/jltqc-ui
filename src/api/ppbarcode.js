import request from '@/utils/request'

export function findAllByModid(modid) {
  return request({
    url: '/ppbarcode/findAllByModid',
    method: 'get',
    params: { modid: modid }
  })
}

export function findByBarcode(barcode) {
  return request({
    url: '/ppbarcode/findByBarcode',
    method: 'get',
    params: { barcode: barcode }
  })
}


