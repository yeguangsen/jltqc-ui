import request from '@/utils/request'

export function queryByPage(data) {
  return request({
    url: '/selectMomorder/queryByPage',
    method: 'post',
    data
  })
}

