import request from '@/utils/request'

export function save(data) {
  return request({
    url: '/baseDepartment/save',
    method: 'post',
    data
  })
}

export function getMenus(params) {
  return request({
    url: '/baseDepartment/getMenus',
    method: 'post',
    params
  })
}
export function list() {
  return request({
    url: '/baseDepartment/list',
    method: 'get'
  })
}

export function getTree(params) {
  return request({
    url: '/baseDepartment/getTree',
    method: 'get'
  })
}
export function query(data) {
  return request({
    url: '/baseDepartment/query',
    method: 'post',
    data
  })
}

export function delbyid(id) {
  return request({
    url: '/baseDepartment/delbyid',
    method: 'get',
    params: { id: id }
  })
}
