import request from '@/utils/request'

export function save(data) {
  return request({
    url: '/supplier/save',
    method: 'post',
    data
  })
}

export function query(data) {
  return request({
    url: '/supplier/query',
    method: 'post',
    data
  })
}

export function delbyid(id) {
  return request({
    url: '/supplier/del',
    method: 'get',
    params: { id: id }
  })
}
export function crmCustomerPoolSetListAPI(id) {
  return request({
    url: '/supplier/del',
    method: 'get',
    params: { id: id }
  })
}
export function get(id) {
  return request({
    url: '/supplier/get',
    method: 'get',
    params: { id: id }
  })
}
export function getSupplierByCvencode(cvencode) {
  return request({
    url: '/supplier/getSupplierByCvencode',
    method: 'get',
    params: { cvencode: cvencode }
  })
}
export function getLast(id) {
  return request({
    url: '/supplier/getLast',
    method: 'get',
    params: { id: id }
  })
}
export function getNext(id) {
  return request({
    url: '/supplier/getNext',
    method: 'get',
    params: { id: id }
  })
}

