import request from '@/utils/request'

export function query(data) {
  return request({
    url: '/rpInCome/query',
    method: 'post',
    data
  })
}

export function save(data) {
  return request({
    url: '/rpInCome/save',
    method: 'post',
    data
  })
}
export function delbyid(id) {
  return request({
    url: '/rpInCome/del',
    method: 'get',
    params: { id: id }
  })
}
export function finish(id) {
  return request({
    url: '/rpInCome/finish',
    method: 'get',
    params: { id: id }
  })
}
export function crmCustomerPoolSetListAPI(id) {
  return request({
    url: '/rpInCome/del',
    method: 'get',
    params: { id: id }
  })
}
export function get(id) {
  return request({
    url: '/rpInCome/get',
    method: 'get',
    params: { id: id }
  })
}
export function findRpIncomeByBarcode(cbarcode) {
  return request({
    url: '/rpInCome/findRpIncomeByBarcode',
    method: 'get',
    params: { cbarcode: cbarcode }
  })
}
