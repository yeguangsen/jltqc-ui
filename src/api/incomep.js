import request from '@/utils/request'

export function query(data) {
  return request({
    url: '/incomep/query',
    method: 'post',
    data
  })
}
export function findAll(data) {
  return request({
    url: '/incomep/findAll',
    method: 'post',
    data
  })
}

export function get(id) {
  return request({
    url: '/incomep/get',
    method: 'get',
    params: { id: id }
  })
}

