import request from '@/utils/request'

export function save(data) {
  return request({
    url: '/quality/save',
    method: 'post',
    data
  })
}

export function query(data) {
  return request({
    url: '/quality/query',
    method: 'post',
    data
  })
}

export function delbyid(id) {
  return request({
    url: '/quality/del',
    method: 'get',
    params: { id: id }
  })
}
export function crmCustomerPoolSetListAPI(id) {
  return request({
    url: '/quality/del',
    method: 'get',
    params: { id: id }
  })
}
export function get(id) {
  return request({
    url: '/quality/get',
    method: 'get',
    params: { id: id }
  })
}
export function getLast(id) {
  return request({
    url: '/quality/getLast',
    method: 'get',
    params: { id: id }
  })
}
export function getNext(id) {
  return request({
    url: '/quality/getNext',
    method: 'get',
    params: { id: id }
  })
}

