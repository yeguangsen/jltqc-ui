import request from '@/utils/request'

export function save(data) {
  return request({
    url: '/baseQuota/save',
    method: 'post',
    data
  })
}

export function query(data) {
  return request({
    url: '/baseQuota/query',
    method: 'post',
    data
  })
}

export function delbyid(id) {
  return request({
    url: '/baseQuota/del',
    method: 'get',
    params: { id: id }
  })
}
export function crmCustomerPoolSetListAPI(id) {
  return request({
    url: '/baseQuota/delbyid',
    method: 'get',
    params: { id: id }
  })
}



