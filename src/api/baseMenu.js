import request from '@/utils/request'

export function save(data) {
  return request({
    url: '/baseMenu/save',
    method: 'post',
    data
  })
}

export function getMenus(params) {
  return request({
    url: '/baseMenu/getMenus',
    method: 'post',
    params
  })
}
export function list() {
  return request({
    url: '/baseMenu/list',
    method: 'get'
  })
}

export function getTree(params) {
  return request({
    url: '/baseMenu/getTree',
    method: 'get'
  })
}

export function delbyid(id) {
  return request({
    url: '/baseMenu/delbyid',
    method: 'get',
    params: { id: id }
  })
}

export function query(data) {
  return request({
    url: '/baseMenu/query',
    method: 'post',
    data
  })
}
