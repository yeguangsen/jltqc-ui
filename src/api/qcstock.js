import request from '@/utils/request'

export function u8query(data) {
  return request({
    url: '/qcStock/query',
    method: 'post',
    data
  })
}
export function query(data) {
  return request({
    url: '/qcStock/query',
    method: 'post',
    data
  })
}
export function save(data) {
  return request({
    url: '/qcStock/save',
    method: 'post',
    data
  })
}
export function delbyid(id) {
  return request({
    url: '/qcStock/del',
    method: 'get',
    params: { id: id }
  })
}
export function crmCustomerPoolSetListAPI(id) {
  return request({
    url: '/qcStock/del',
    method: 'get',
    params: { id: id }
  })
}
export function get(id) {
  return request({
    url: '/qcStock/get',
    method: 'get',
    params: { id: id }
  })
}
export function getLast(id) {
  return request({
    url: '/qcStock/getLast',
    method: 'get',
    params: { id: id }
  })
}
export function getNext(id) {
  return request({
    url: '/qcStock/getNext',
    method: 'get',
    params: { id: id }
  })
}
