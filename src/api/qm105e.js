import request from '@/utils/request'

export function findAll(id) {
  return request({
    url: '/qm105e/findAll',
    method: 'get'
  })
}

