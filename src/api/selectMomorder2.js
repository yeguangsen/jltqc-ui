import request from '@/utils/request'

export function queryByPage(data) {
  return request({
    url: '/selectMomorder2/queryByPage',
    method: 'post',
    data
  })
}

