import request from '@/utils/request'

export function save(data) {
  return request({
    url: '/customer/save',
    method: 'post',
    data
  })
}

export function query(data) {
  return request({
    url: '/customer/query',
    method: 'post',
    data
  })
}

export function delbyid(id) {
  return request({
    url: '/customer/delbyid',
    method: 'get',
    params: { id: id }
  })
}
export function crmCustomerPoolSetListAPI(id) {
  return request({
    url: '/customer/delbyid',
    method: 'get',
    params: { id: id }
  })
}



