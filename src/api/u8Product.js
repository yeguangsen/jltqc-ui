import request from '@/utils/request'

export function query(data) {
  return request({
    url: '/u8Inventory/query',
    method: 'post',
    data
  })
}

