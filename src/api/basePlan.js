import request from '@/utils/request'

export function save(data) {
  return request({
    url: '/basePlan/save',
    method: 'post',
    data
  })
}

export function query(data) {
  return request({
    url: '/basePlan/query',
    method: 'post',
    data
  })
}

export function delbyid(id) {
  return request({
    url: '/basePlan/del',
    method: 'get',
    params: { id: id }
  })
}
export function crmCustomerPoolSetListAPI(id) {
  return request({
    url: '/basePlan/delbyid',
    method: 'get',
    params: { id: id }
  })
}



