import request from '@/utils/request'

export function u8query(data) {
  return request({
    url: '/workflow/query',
    method: 'post',
    data
  })
}
export function query(data) {
  return request({
    url: '/workflow/query',
    method: 'post',
    data
  })
}
export function queryAll(data) {
  return request({
    url: '/workflow/queryAll',
    method: 'post',
    data
  })
}
export function save(data) {
  return request({
    url: '/workflow/save',
    method: 'post',
    data
  })
}
export function delbyid(id) {
  return request({
    url: '/workflow/del',
    method: 'get',
    params: { id: id }
  })
}
export function get(id) {
  return request({
    url: '/workflow/get',
    method: 'get',
    params: { id: id }
  })
}
export function getLast(id) {
  return request({
    url: '/workflow/getLast',
    method: 'get',
    params: { id: id }
  })
}
export function getNext(id) {
  return request({
    url: '/workflow/getNext',
    method: 'get',
    params: { id: id }
  })
}
