import request from '@/utils/request'

export function save(data) {
  return request({
    url: '/baseUser/save',
    method: 'post',
    data
  })
}

export function query(data) {
  return request({
    url: '/baseUser/query',
    method: 'post',
    data
  })
}

export function delbyid(id) {
  return request({
    url: '/baseUser/del',
    method: 'get',
    params: { id: id }
  })
}
export function crmCustomerPoolSetListAPI(id) {
  return request({
    url: '/baseUser/delbyid',
    method: 'get',
    params: { id: id }
  })
}

