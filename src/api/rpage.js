import request from '@/utils/request'

export function query(data) {
  return request({
    url: '/rpAge/query',
    method: 'post',
    data
  })
}
export function save(data) {
  return request({
    url: '/rpAge/save',
    method: 'post',
    data
  })
}
export function delbyid(id) {
  return request({
    url: '/rpAge/del',
    method: 'get',
    params: { id: id }
  })
}
export function get(id) {
  return request({
    url: '/rpAge/get',
    method: 'get',
    params: { id: id }
  })
}
export function generateWorkflow(id) {
  return request({
    url: '/rpAge/generateWorkflow',
    method: 'get',
    params: { id: id }
  })
}
export function getLast(id) {
  return request({
    url: '/rpAge/getLast',
    method: 'get',
    params: { id: id }
  })
}
export function getNext(id) {
  return request({
    url: '/rpAge/getNext',
    method: 'get',
    params: { id: id }
  })
}
