import request from '@/utils/request'

export function save(data) {
  return request({
    url: '/report/save',
    method: 'post',
    data
  })
}

export function query(data) {
  return request({
    url: '/report/query',
    method: 'post',
    data
  })
}

export function delbyid(id) {
  return request({
    url: '/report/del',
    method: 'get',
    params: { id: id }
  })
}
export function crmCustomerPoolSetListAPI(id) {
  return request({
    url: '/report/del',
    method: 'get',
    params: { id: id }
  })
}
export function get(id) {
  return request({
    url: '/report/get',
    method: 'get',
    params: { id: id }
  })
}
export function getLast(id, mold) {
  return request({
    url: '/report/getLast',
    method: 'get',
    params: { id: id, mold: mold }
  })
}
export function getNext(id, mold) {
  return request({
    url: '/report/getNext',
    method: 'get',
    params: { id: id, mold: mold }
  })
}
