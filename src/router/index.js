import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'
import QualityEdit from '@/views/quality/edit.vue'
import QualityView from '@/views/quality/view.vue'
import inComeView from '@/views/inCome/view.vue'
import outComeView from '@/views/outCome/view.vue'
import qcProduceView from '@/views/qcProduce/view.vue'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/',
    component: Layout,
    redirect: 'dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '首页', icon: 'dashboard' }
    }]
  }, {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  }, {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  }
]
export const notFondRouter = [
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]
const createRouter = () => new Router({
  mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

// 异步挂载的路由
// 动态需要根据权限加载的路由表
export const asyncRouterMap = [
  {
    path: '/formwork',
    component: Layout,
    redirect: '/formwork/ipqcjyFormworkEdit',
    meta: { title: '质检模板', icon: 'el-icon-s-help', keepAlive: true },
    children: [{
      path: 'ipqcjyFormworkEdit',
      name: 'ipqcjyFormworkEdit',
      component: () => import('@/views/formwork/edit'),
      meta: { title: 'IPQC检验新建', icon: 'dashboard' }
    },
    {
      path: 'ipqcjyFormworkIndex',
      name: 'ipqcjyFormworkIndex',
      component: () => import('@/views/formwork/index'),
      meta: { title: 'IPQC检验列表', icon: 'dashboard' }
    },
    {
      path: 'ipqcxjFormworkEdit',
      name: 'ipqcxjFormworkEdit',
      component: () => import('@/views/formwork/edit'),
      meta: { title: 'IPQC巡验新建', icon: 'dashboard' }
    },
    {
      path: 'ipqcxjFormworkIndex',
      name: 'ipqcxjFormworkIndex',
      component: () => import('@/views/formwork/index'),
      meta: { title: 'IPQC巡验列表', icon: 'dashboard' }
    },
    {
      path: 'produceFormworkEdit',
      name: 'produceFormworkEdit',
      component: () => import('@/views/formwork/edit'),
      meta: { title: '产品检验新建', icon: 'dashboard' }
    },
    {
      path: 'produceFormworkIndex',
      name: 'produceFormworkIndex',
      component: () => import('@/views/formwork/index'),
      meta: { title: '产品检验列表', icon: 'dashboard' }
    },
    {
      path: 'oldproduceFormworkEdit',
      name: 'oldproduceFormworkEdit',
      component: () => import('@/views/formwork/edit'),
      meta: { title: '产品老化新建', icon: 'dashboard' }
    },
    {
      path: 'oldproduceFormworkIndex',
      name: 'oldproduceFormworkIndex',
      component: () => import('@/views/formwork/index'),
      meta: { title: '产品老化列表', icon: 'dashboard' }
    },
    {
      path: 'produceoutFormworkEdit',
      name: 'produceoutFormworkEdit',
      component: () => import('@/views/formwork/edit'),
      meta: { title: '产品出厂新建', icon: 'dashboard' }
    },
    {
      path: 'produceoutFormworkIndex',
      name: 'produceoutFormworkIndex',
      component: () => import('@/views/formwork/index'),
      meta: { title: '产品出厂列表', icon: 'dashboard' }
    },
    {
      path: 'customerFormworkEdit',
      name: 'customerFormworkEdit',
      component: () => import('@/views/formwork/edit'),
      meta: { title: '顾客投诉新建', icon: 'dashboard' }
    },
    {
      path: 'customerFormworkIndex',
      name: 'customerFormworkIndex',
      component: () => import('@/views/formwork/index'),
      meta: { title: '顾客投诉列表', icon: 'dashboard' }
    },
    {
      path: 'pphandleFormworkEdit',
      name: 'pphandleFormworkEdit',
      component: () => import('@/views/formwork/edit'),
      meta: { title: 'PP每日维修新建', icon: 'dashboard' }
    },
    {
      path: 'pphandleFormworkIndex',
      name: 'pphandleFormworkIndex',
      component: () => import('@/views/formwork/index'),
      meta: { title: 'PP每日维修列表', icon: 'dashboard' }
    },
    {
      path: 'ppfirstFormworkEdit',
      name: 'ppfirstFormworkEdit',
      component: () => import('@/views/formwork/edit'),
      meta: { title: 'PP组装首检新建', icon: 'dashboard' }
    },
    {
      path: 'ppfirstFormworkIndex',
      name: 'ppfirstFormworkIndex',
      component: () => import('@/views/formwork/index'),
      meta: { title: 'PP组装首检列表', icon: 'dashboard' }
    }]
  },
  {
    path: '/report',
    component: Layout,
    redirect: '/report/ipqcjyReportEdit',
    meta: { title: '质检记录', icon: 'el-icon-s-help', keepAlive: true },
    children: [{
      path: 'ipqcjyReportEdit',
      name: 'ipqcjyReportEdit',
      component: () => import('@/views/report/edit'),
      meta: { title: 'IPQC检验新建', icon: 'dashboard' }
    },
    {
      path: 'ipqcjyReportIndex',
      name: 'ipqcjyReportIndex',
      component: () => import('@/views/report/index'),
      meta: { title: 'IPQC检验列表', icon: 'dashboard' }
    },
    {
      path: 'ipqcxjReportEdit',
      name: 'ipqcxjReportEdit',
      component: () => import('@/views/report/edit'),
      meta: { title: 'IPQC巡验新建', icon: 'dashboard' }
    },
    {
      path: 'ipqcxjReportIndex',
      name: 'ipqcxjReportIndex',
      component: () => import('@/views/report/index'),
      meta: { title: 'IPQC巡验列表', icon: 'dashboard' }
    },
    {
      path: 'produceReportEdit',
      name: 'produceReportEdit',
      component: () => import('@/views/report/edit'),
      meta: { title: '产品检验新建', icon: 'dashboard' }
    },
    {
      path: 'produceReportIndex',
      name: 'produceReportIndex',
      component: () => import('@/views/report/index'),
      meta: { title: '产品检验列表', icon: 'dashboard' }
    },
    {
      path: 'oldproduceReportEdit',
      name: 'oldproduceReportEdit',
      component: () => import('@/views/report/edit'),
      meta: { title: '产品老化新建', icon: 'dashboard' }
    },
    {
      path: 'oldproduceReportIndex',
      name: 'oldproduceReportIndex',
      component: () => import('@/views/report/index'),
      meta: { title: '产品老化列表', icon: 'dashboard' }
    },
    {
      path: 'produceoutReportEdit',
      name: 'produceoutReportEdit',
      component: () => import('@/views/report/edit'),
      meta: { title: '产品出厂新建', icon: 'dashboard' }
    },
    {
      path: 'produceoutReportIndex',
      name: 'produceoutReportIndex',
      component: () => import('@/views/report/index'),
      meta: { title: '产品出厂列表', icon: 'dashboard' }
    },
    {
      path: 'customerReportEdit',
      name: 'customerReportEdit',
      component: () => import('@/views/report/edit'),
      meta: { title: '顾客投诉新建', icon: 'dashboard' }
    },
    {
      path: 'customerReportIndex',
      name: 'customerReportIndex',
      component: () => import('@/views/report/index'),
      meta: { title: '顾客投诉列表', icon: 'dashboard' }
    },
    {
      path: 'pphandleReportEdit',
      name: 'pphandleReportEdit',
      component: () => import('@/views/report/edit'),
      meta: { title: 'PP每日维修新建', icon: 'dashboard' }
    },
    {
      path: 'pphandleReportIndex',
      name: 'pphandleReportIndex',
      component: () => import('@/views/report/index'),
      meta: { title: 'PP每日维修列表', icon: 'dashboard' }
    },
    {
      path: 'ppfirstReportEdit',
      name: 'ppfirstReportEdit',
      component: () => import('@/views/report/edit'),
      meta: { title: 'PP组装首件新建', icon: 'dashboard' }
    },
    {
      path: 'ppfirstReportIndex',
      name: 'ppfirstReportIndex',
      component: () => import('@/views/report/index'),
      meta: { title: 'PP组装首件列表', icon: 'dashboard' }
    }]
  },
  {
    path: '/base',
    component: Layout,
    redirect: '/base/basePlan',
    meta: { title: '基础数据', icon: 'el-icon-s-help', keepAlive: true },
    children: [
      {
        path: 'basePlan',
        name: 'BasePlan',
        component: () => import('@/views/basePlan/index'),
        meta: { title: '项目管理', icon: 'tree', keepAlive: true }
      },
      {
        path: 'baseQuota',
        name: 'BaseQuota',
        component: () => import('@/views/baseQuota/index'),
        meta: { title: '指标管理', icon: 'tree' }
      },
      {
        path: 'qmilcode',
        name: 'qmilcode',
        component: () => import('@/views/qmilcode/index'),
        meta: { title: '质检主数据表', icon: 'tree' }
      },
      {
        path: 'qm105e',
        name: 'qm105e',
        component: () => import('@/views/qm105e/index'),
        meta: { title: '样本量字码表', icon: 'tree' }
      }
    ]
  },
  {
    path: '/complaint',
    component: Layout,
    redirect: '/complaint/complaintItem',
    meta: { title: '客诉', icon: 'el-icon-s-help', keepAlive: true },
    children: [{
      path: 'complaintItem',
      name: 'complaintItem',
      component: () => import('@/views/complaint/item'),
      meta: { title: '客诉内容', icon: 'dashboard' }
    },
    {
      path: 'complaintEdit',
      name: 'complaintEdit',
      component: () => import('@/views/complaint/edit'),
      meta: { title: '客诉编辑', icon: 'dashboard' }
    },
    {
      path: 'complaintIndex',
      name: 'complaintIndex',
      component: () => import('@/views/complaint/index'),
      meta: { title: '客诉列表', icon: 'dashboard' }
    },
    { path: 'complaintView',
      component: () => import('@/views/complaint/view'),
      hidden: true
    }]
  },
  {
    path: '/supplier',
    component: Layout,
    redirect: '/supplier/supplierEdit',
    meta: { title: '供应商质检', icon: 'el-icon-s-help', keepAlive: true },
    children: [{
      path: 'supplierEdit',
      name: 'SupplierEdit',
      component: () => import('@/views/supplier/edit'),
      meta: { title: '供应商新建', icon: 'dashboard' }
    },
    {
      path: 'supplierIndex',
      name: 'SupplierIndex',
      component: () => import('@/views/supplier/index'),
      meta: { title: '供应商列表', icon: 'dashboard' }
    },
    { path: 'supplierView',
      component: () => import('@/views/supplier/view'),
      hidden: true
    }]
  },
  {
    path: '/workflow',
    component: Layout,
    redirect: '/workflow/workflowChart',
    meta: { title: '审批流程', icon: 'el-icon-s-help', keepAlive: true },
    children: [{
      path: 'workflowChart',
      name: 'workflowChart',
      component: () => import('@/views/workflow/chart'),
      meta: { title: '生产审批流程图', icon: 'dashboard' }
    },
    {
      path: 'workflowView',
      name: 'workflowView',
      component: () => import('@/views/workflow/view'),
      meta: { title: '生产审批记录', icon: 'dashboard' }
    },
    {
      path: 'workflowIndex',
      name: 'workflowIndex',
      component: () => import('@/views/workflow/index'),
      meta: { title: '生产审批管理', icon: 'dashboard' }
    },
    { path: 'workflowEdit',
      component: () => import('@/views/workflow/edit'),
      hidden: true
    }]
  }, {
    path: '/project',
    component: Layout,
    redirect: '/project/projectIndex',
    name: 'base',
    meta: { title: '方案管理', icon: 'el-icon-s-help', keepAlive: true },
    children: [
      {
        path: 'projectEdit',
        name: 'ProjectEdit',
        component: () => import('@/views/project/edit'),
        meta: { title: '方案管理', icon: 'tree' }
      },
      {
        path: 'projectIndex',
        name: 'ProjectIndex',
        component: () => import('@/views/project/index'),
        meta: { title: '方案列表', icon: 'tree' }
      }
    ]
  },
  {
    path: '/quality',
    component: Layout,
    redirect: '/quality/inComeEdit',
    meta: { title: '质检管理', icon: 'el-icon-s-help' },
    children: [
      // {
      //   path: 'quality',
      //   name: 'quality',
      //   component: () => import('@/views/quality/index'),
      //   meta: { title: '质检管理', icon: 'tree' }
      // },
      {
        path: 'inComeEdit',
        name: 'inComeEdit',
        component: () => import('@/views/inCome/edit'),
        meta: { title: '来料质检', icon: 'tree' }
      },
      { path: 'QualityEdit',
        component: QualityEdit,
        hidden: true
      },
      { path: 'QualityEdit',
        component: QualityEdit,
        hidden: true
      },
      { path: 'QualityView',
        component: QualityView,
        hidden: true
      },
      {
        path: 'inComeIndex',
        name: 'inComeIndex',
        component: () => import('@/views/inCome/index'),
        meta: { title: '来料质检记录', icon: 'tree' }
      },
      {
        path: 'rpComeEdit',
        name: 'rpComeEdit',
        component: () => import('@/views/rpincome/edit'),
        meta: { title: '采购成品质检', icon: 'tree' }
      },
      {
        path: 'rpinComeIndex',
        name: 'rpinComeIndex',
        component: () => import('@/views/rpincome/index'),
        meta: { title: '采购成品记录', icon: 'tree' }
      },
      {
        path: 'rpproduceEdit',
        name: 'rpproduceEdit',
        component: () => import('@/views/rpproduce/edit'),
        meta: { title: '产品检验填写', icon: 'tree' }
      },
      {
        path: 'rpproduceIndex',
        name: 'rpproduceIndex',
        component: () => import('@/views/rpproduce/index'),
        meta: { title: '产品检验记录', icon: 'tree' }
      },
      {
        path: 'rppstockEdit',
        name: 'rppstockEdit',
        component: () => import('@/views/rpstock/edit'),
        meta: { title: '库存检验填写', icon: 'tree' }
      },
      {
        path: 'rpstockIndex',
        name: 'rpstockIndex',
        component: () => import('@/views/rpstock/index'),
        meta: { title: '库存检验记录', icon: 'tree' }
      },
      {
        path: 'rpageEdit',
        name: 'rpageEdit',
        component: () => import('@/views/rpage/edit'),
        meta: { title: '产品老化填写', icon: 'tree' }
      },
      {
        path: 'rpageIndex',
        name: 'rpageIndex',
        component: () => import('@/views/rpage/index'),
        meta: { title: '产品老化记录', icon: 'tree' }
      },
      {
        path: 'rpxunjianEdit',
        name: 'rpxunjianEdit',
        component: () => import('@/views/rpxunjian/edit'),
        meta: { title: 'IPQC巡检填写', icon: 'tree' }
      },
      {
        path: 'rpxunjianIndex',
        name: 'rpxunjianIndex',
        component: () => import('@/views/rpxunjian/index'),
        meta: { title: 'IPQC巡检记录', icon: 'tree' }
      },
      { path: 'inComeView',
        component: inComeView,
        hidden: true
      },
      {
        path: 'outComeEdit',
        name: 'outComeEdit',
        component: () => import('@/views/outCome/edit'),
        meta: { title: '销售质检', icon: 'tree' }
      },
      {
        path: 'outComeIndex',
        name: 'outComeIndex',
        component: () => import('@/views/outCome/index'),
        meta: { title: '销售列表', icon: 'tree' }
      },
      { path: 'outComeView',
        component: outComeView,
        hidden: true
      },
      {
        path: 'qcProduceEdit',
        name: 'qcProduceEdit',
        component: () => import('@/views/qcProduce/edit'),
        meta: { title: '生产质检', icon: 'tree' }
      },
      {
        path: 'qcProduceIndex',
        name: 'qcProduceIndex',
        component: () => import('@/views/qcProduce/index'),
        meta: { title: '生产列表', icon: 'tree' }
      },
      { path: 'qcProduceView',
        component: qcProduceView,
        hidden: true
      },
      {
        path: 'qcStockEdit',
        name: 'qcStockEdit',
        component: () => import('@/views/qcStock/edit'),
        meta: { title: '库存质检', icon: 'tree' }
      },
      {
        path: 'qcStockIndex',
        name: 'qcStockIndex',
        component: () => import('@/views/qcStock/index'),
        meta: { title: '库存质检列表', icon: 'tree' }
      },
      { path: 'qcStockView',
        component: () => import('@/views/qcStock/view'),
        hidden: true
      }
    ]
  },
  {
    path: '/systemManger',
    component: Layout,
    redirect: '/systemManger/baseUser',
    name: 'systemManger',
    meta: { title: '系统管理', icon: 'el-icon-s-help', role: ['admin'] },
    children: [
      {
        path: 'baseUser',
        name: 'baseUser',
        component: () => import('@/views/baseUser/index'),
        meta: { title: '用户管理', icon: 'tree' }
      },
      {
        path: 'baseDepartment',
        name: 'baseDepartment',
        component: () => import('@/views/baseDepartment/index'),
        meta: { title: '部门管理', icon: 'tree' }
      },
      {
        path: 'baseMenu',
        name: 'baseMenu',
        component: () => import('@/views/baseMenu/index'),
        meta: { title: '菜单管理', icon: 'tree' }
      }
    ]
  }

]

export default router
