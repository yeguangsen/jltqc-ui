import { asyncRouterMap, constantRoutes, notFondRouter } from '@/router/index'
// store/permission.js
function hasPermission(roles, route) {
  if (route.path) {
    if (roles === 'common') {
      return route.path !== 'baseUser' && route.path !== 'baseMenu' && route.path !== 'baseDepartment'
    }
    return roles.some(role => route.path === role.path)
  } else {
    return true
  }
}

const permission = {
  state: {
    routers: [],
    addRouters: []
  },
  mutations: {
    SET_ROUTERS: (state, accessedRouters) => {
      state.addRouters = accessedRouters
      state.routers = accessedRouters
    }
  },
  actions: {
    GenerateRoutes({ commit }, data) {
      return new Promise(resolve => {
        const { roles } = data
        const accessedRouters = asyncRouterMap.filter(v => {
          if (roles.indexOf('admin') >= 0) return true
          if (v.children && v.children.length > 0) {
            v.children = v.children.filter(child => {
              if (hasPermission(roles, child)) {
                return child
              }
              return false
            })
          }
          if (v.children && v.children.length > 0) {
            return v
          }
          return false
        })
        const routt = []
        for (const r of constantRoutes) {
          routt.push(r)
        }
        for (const r of accessedRouters) {
          routt.push(r)
        }
        for (const r of notFondRouter) {
          routt.push(r)
        }
        commit('SET_ROUTERS', routt)
        resolve()
      })
    }
  }
}
export default permission
